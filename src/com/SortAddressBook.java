package com;

import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * Created by Евгений on 29.07.2017.
 */
public class SortAddressBook  {
    private List<AddressBook> addressBooks;
    Scanner scanner = new Scanner(System.in);

    public SortAddressBook(List<AddressBook> addressBooks) {
        this.addressBooks = addressBooks;
    }

    public List sortFirstname() {

        List<String> strings =  addressBooks.stream()
                .sorted((o1, o2) -> o1.getFirstname().compareTo(o2.getFirstname()))
                .map(addressBook -> addressBook.getFirstname() + " " + addressBook.getSurname() + " " + addressBook.getPhoneNumber())
                .collect(Collectors.toList());

        System.out.println(strings);

        return strings;
    }
    public List sortPhone(){
        List<String> strings =  addressBooks.stream()
                .sorted((o1, o2) -> o1.getPhoneNumber().compareTo(o2.getPhoneNumber()))
                .map(addressBook -> addressBook.getFirstname() + " " + addressBook.getSurname() + " " + addressBook.getPhoneNumber())
                .collect(Collectors.toList());
        System.out.println(strings);
        return strings;
    }



    public List sortSurname() {

        List<String> strings =
                addressBooks.stream()
                        .sorted((o1, o2) -> o1.getSurname().compareTo(o2.getSurname()))
                        .map(addressBook -> addressBook.getFirstname() + " " + addressBook.getSurname() + " " + addressBook.getPhoneNumber())
                        .collect(Collectors.toList());
        System.out.println(strings);
        return strings;
    }
    public void addInfo(){
        System.out.println("Напишите имя, фамилия и номер телефона через  энтер");
        String name = scanner.next();
        String lastname = scanner.next();
        String phoneM = scanner.next();
        addressBooks.add(new AddressBook(name, lastname, phoneM));
        System.out.println(" Ваши данные добавлены");
    }
//    public void seachInfo(){
//        System.out.println("Напишите имя, фамилия и номер телефона через  энтер");
//        String name = scanner.next();
//        String lastname = scanner.next();
//        String phoneM = scanner.next();
//if (){
//
//}
          //  }

    public void delInfo(){
        Iterator<AddressBook> iterator = addressBooks.iterator();
        System.out.println("Напишите имя, фамилия и номер телефона через  энтер");
        String nameDel = scanner.next();
        String lastnameDel = scanner.next();
        String phoneMDel = scanner.next();
        if(iterator.hasNext()){
            AddressBook next = iterator.next();
            if((next.getFirstname().equals(nameDel)) && (next.getSurname().equals(lastnameDel)) && (next.getPhoneNumber().equals(phoneMDel))){
                iterator.remove();
                System.out.println(" Ваши данные Удалены");
            }else System.out.println("Error");
        }



    }
    public void m(){
        System.out.println("-------------------------------------------------------------------------");
        System.out.println("| Если Вы хотите отсортировать список по имени напишите: name            |");
        System.out.println("| Если Вы хотите отсортировать список по фамилии напишите: surname       |");
        System.out.println("| Если Вы хотите отсортировать список по номеру телефона нашишите: phone |");
        System.out.println("| Если Вы хотите добавить данные напишите: add -> имя, фамилия мобила    |");
        System.out.println("| Если Вы хотите удалить данные напишите: del -> имя, фамилия мобила     |");
        System.out.println("| Если Вы хотите выйти напишите: exit                                    |");
        System.out.println("-------------------------------------------------------------------------");
    }






}
