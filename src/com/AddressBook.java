package com;


/**
 * Created by Евгений on 27.07.2017.
 */
public class AddressBook {

    private String firstname;
    private String surname;
    private String phoneNumber;

    AddressBook(String firstname, String surname, String phoneNumber){
        this.firstname = firstname;
        this.surname = surname;
        this.phoneNumber = phoneNumber;

    }

    public String getFirstname() {
        return firstname;
    }

    public String getSurname() {
        return surname;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

//    public void setFirstname(String firstname) {
//        this.firstname = firstname;
//    }
//
//    public void setSurname(String surname) {
//        this.surname = surname;
//    }
//
//    public void setPhoneNumber(String phoneNumber) {
//        this.phoneNumber = phoneNumber;
//    }
}






