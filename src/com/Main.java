package com;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import java.util.Scanner;


public class Main {




    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        List<AddressBook> addressBooks = new ArrayList<>();

        addressBooks.add(new AddressBook("Zhenya", "Shapovalov", "+380669899400"));
        addressBooks.add(new AddressBook("Natalia", "Glushko", "+380994694445"));
        addressBooks.add(new AddressBook("Vetalii", "Bezdvornui", "+380661715808"));
        addressBooks.add(new AddressBook("Bolnica", "Ckoraia", "103"));
        addressBooks.add(new AddressBook("Viacheslav ", "Shapovalov", "+380954133407"));
        addressBooks.add(new AddressBook("Anton", "Petrov", "+380505555555"));
        addressBooks.add(new AddressBook("a", "a", "a"));

        SortAddressBook st = new SortAddressBook(addressBooks);

        while (true) {

            st.m();//меню

            String info = scanner.next();
            if (info.equals("name")) {
                st.sortFirstname();//сортировка по имени
            }
            else if (info.equals("surname")) { //сортировка по фамилии
                st.sortSurname();

            }
            else if (info.equals("phone")) {//сортировка по телефону
                 st.sortPhone();
            }
            else if (info.equals("add")){//добавление нового человека
                st.addInfo();
            }
            else if (info.equals("del")){//удаление данных
                st.delInfo();
            }
            else if (info.equals("exit")) {

                System.out.println("Goodbye");
                return;
            }
            else {
                System.out.println("Введите правильные команды!");

            }

        }
    }
}